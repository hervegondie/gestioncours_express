var express = require('express');
var router = express.Router();
var pages = require('../modules/pages.js');


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { 
    titre: 'École de formation continue',
    description: 'Une école à la pointe de le technologie',
    texte: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
     });
});

router.get('/formation', function(req, res, next){
  res.render('pages', 
  {title: 'Formations', 
  description: "Liste des formations",
  pages:pages.allPages,});
});

router.get('/formation/ :id', function(req, res, next){
  res.render('pages', {pages: pages.getPageById(req.params.id)});
});

router.get('/contact', function(req,res,next){
  res.render('contact');
})

router.post('/contact', function(req,res,next){
  let etudiant = pages.addStudent(req.body)
  if(etudiant){
    res.render('pages',{ title: 'Liste des étudiants', description: "Les étudiants de la formation continue", pages: pages.allStudent } )
  }
})

router.get('/blog', function(req, res, next){
  res.render('pages', 
  {title: 'Article', 
  description: "Liste des Articles",
  pages:pages.allArticle,});
});

router.get('/blog/ :id', function(req, res, next){
  res.render('pages', {pages: pages.getArticleById(req.params.id)});
});
  
module.exports = router;
